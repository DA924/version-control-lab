#include <iostream>
#include <string>
#include <fstream>



using namespace std;

//In the program, the user will choose whether to make purchase or modify the inventory

//making a purchase will dynamically store the basket until the user is done with purchase

//the inventory allows authorised users to view and add to the stocks, as well as update stocks. 
//it also allows authorised users to view sales overtime

struct item{
    string stockID;
    string stockName;
    int price;
    int quantity;
};



item getStock(ifstream &read){
    item o;
    read.get();
    getline(read, o.stockID);
    getline(read, o.stockName);
    read>> o.price;
    read>> o.quantity;
    return o;
    
}

void printStock(){
    cout<<"\nID----Name----Price----Quantity";
    cout<<"\n++    ++++    +++++    ++++++++";
    
    ifstream read;
    read.open("stock.txt", ios::in);
    item i;
    
    i = getStock(read);
    while(!read.eof()){
        
        cout<<"\n\n"<<i.stockID<<"----"<<i.stockName<<"----$"<<i.price<<"----"<<i.quantity;
        i = getStock(read);
    }
    
    
}

void printItem(string s){

    cout<<"\nID----Name----Price----Quantity";
    cout<<"\n++    ++++    +++++    ++++++++\n";
    
    ifstream read;
    read.open("stock.txt", ios::in);
    item i;
    
    i = getStock(read);
    while(!read.eof()){
        
        if (s == i.stockID){
            cout<<"\n\n"<<i.stockID<<"----"<<i.stockName<<"----$"<<i.price<<"----"<<i.quantity<<"\n";
        }

        i = getStock(read);
    }
    
}



void addStock(string def){
    ofstream write("stock.txt", ios::app);
    
    item i;
    
    cout<<"Enter Stock ID : ";
    cin.get();
    getline(cin, i.stockID);
    cout<<"Enter The name/brand of Item : ";
    getline(cin, i.stockName);
    cout<<"Enter Stock price : ";
    cin>>i.price;
    cout<<"Enter Stock Quantity : ";
    cin>>i.quantity;
    
    write<< def<<i.stockID << endl;
    write<< i.stockName;
    write<< endl << i.price << endl << i.quantity<<endl;
    
    cout<<"ITEM ADDED SUCESSFULLY\n-------------------------------------\n";
    printStock();
    
    return;
}

void updateItem(string def){
    
    string s;
    cout<<"Enter the ID of the product you want to update: \n";
    cin.get();
    getline(cin, s);
    
    ofstream write("temp.txt", ios::app);
    write<<endl;
    
    ifstream read;
    read.open("stock.txt", ios::in);
    
    item i;
    i = getStock(read);
    
    while(!read.eof()){
        if(i.stockID==s){
            printItem(s);
            cout<<"\nEnter new record: \n";
            
            cout<<"Enter Stock ID : ";
            getline(cin, i.stockID);
            cout<<"Enter The name/brand of Item : ";
            getline(cin, i.stockName);
            cout<<"Enter Stock price : ";
            cin>>i.price;
            cout<<"Enter Stock Quantity : ";
            cin>>i.quantity;
             
            write<< def<<i.stockID << endl;
            write<< i.stockName;
            write<< endl << i.price << endl << i.quantity<<endl;
            
        }else{
            write<< i.stockID << endl;
            write<< i.stockName;
            write<< endl << i.price << endl << i.quantity<<endl;
        }
        i = getStock(read);
    }
    write.close();
    read.close();
    remove("stock.txt");
    rename("temp.txt", "stock.txt");
}

//derived classes 

class CD {
    string def = "CD";
    
    public:
    void add(){
        cout<<"Enter Search Details for CD\n";
        cout<<"Data entered for ID will have CD in front of it\n";
        addStock(def);
    }
    
    void update(){
        cout<<"Enter Update Details for CD\n";
        updateItem(def);
    }
};

class DVD{
    string def = "DV";
    
    public:
    void add(){
        cout<<"Enter Search Details for DVD\n";
        cout<<"Data entered for ID will have DV in front of it\n";
        addStock(def);
    }
    
    void update(){
        cout<<"Enter Update Details for DVD\n";
        updateItem(def);
    }
};

class Book{
    string def = "BK";
    
    public:
    void add(){
        cout<<"Enter Search Details for BOOK\n";
        cout<<"Data entered for ID will have BK in front of it\n";
        addStock(def);
    }
    
    void update(){
        cout<<"Enter Update Details for BOOK\n";
        updateItem(def);
    }
};

class Magazine{
    string def = "MG";
    
    public:
    void add(){
        cout<<"Enter Search Details for MAGAZINE\n";
        cout<<"Data entered for ID will have MG in front of it\n";
        addStock(def);
    }
    
    void update(){
        cout<<"Enter Update Details for MAGAZINE\n";
        updateItem(def);
    }
};

void updateActions(){
    int choose;
    
    cout<<"\t\t\t+++++++++++++++++++++++++++++++++++++\n";
    cout<<"\t\t\tSelect which Item you are updating to\n";
    cout<<"\t\t\t+++++++++++++++++++++++++++++++++++++\n\n";
    
    cout<<"\t\t\tSelect [1] for CD\n";
    cout<<"\t\t\tSelect [2] for DVD\n";
    cout<<"\t\t\tSelect [3] for BOOK\n";
    cout<<"\t\t\tSelect [4] for MAGAZINE\n";
    cout<<"\t\t\tSelect [0] to Exit\n\n";
    
    cout<<"\t\t\tSelect your choice: ";
    cin >>choose;
    switch(choose){
        case 1:
        {
            CD cd;
            cd.update();
            updateActions();
        }
        break;
        
        case 2:
        {
            DVD dvd;
            dvd.update();
            updateActions();
        }
        break;
        
        case 3:
        {
            Book b;
            b.update();
            updateActions();
        }
        break;
        
        case 4:
        {
            Magazine m;
            m.update();
            updateActions();
        }
        break;
        
        case 0:
        return;
        break;
        
        default:
        cout<<"\t\t\t+++++++++++++++++++++++++++++++++++++\n";
        cout<<"\t\t\tSelect which Item you are updating to\n";
        cout<<"\t\t\t+++++++++++++++++++++++++++++++++++++\n\n";
        
        cout<<"\t\t\tSelect [1] for CD\n";
        cout<<"\t\t\tSelect [2] for DVD\n";
        cout<<"\t\t\tSelect [3] for BOOK\n";
        cout<<"\t\t\tSelect [4] for MAGAZINE\n";
        cout<<"\t\t\tSelect [0] to Exit\n\n";
        
        cout<<"\t\t\tSelect your choice: ";
        cin >>choose;
        break;
    }
}

void addActions(){
    int choose;
    
    cout<<"\t\t\t+++++++++++++++++++\n";
    cout<<"\t\t\tSelect Item to add?\n";
    cout<<"\t\t\t+++++++++++++++++++\n\n";
    
    cout<<"\t\t\tSelect [1] for CD\n";
    cout<<"\t\t\tSelect [2] for DVD\n";
    cout<<"\t\t\tSelect [3] for BOOK\n";
    cout<<"\t\t\tSelect [4] for MAGAZINE\n";
    cout<<"\t\t\tSelect [0] to Exit\n\n";
    
    cout<<"\t\t\tSelect your choice: ";
    cin >>choose;
    
    switch(choose){
        case 1:
        {
            CD cd;
            cd.add();
            addActions();
        }
        break;
        
        case 2:
        {
            DVD dvd;
            dvd.add();
            addActions();
        }
        break;
        
        case 3:
        {
            Book b;
            b.add();
            addActions();
        }
        break;
        
        case 4:
        {
            Magazine m;
            m.add();
            addActions();
        }
        break;
        
        case 0:
        return;
        break;
        
        default:
        cout<<"\t\t\t+++++++++++++++++++\n";
        cout<<"\t\t\tSelect Item to add?\n";
        cout<<"\t\t\t+++++++++++++++++++\n\n";
        
        cout<<"\t\t\tSelect [1] for CD\n";
        cout<<"\t\t\tSelect [2] for DVD\n";
        cout<<"\t\t\tSelect [3] for BOOK\n";
        cout<<"\t\t\tSelect [4] for MAGAZINE\n";
        cout<<"\t\t\tSelect [0] to Exit\n\n";
        
        cout<<"\t\t\tSelect your choice: ";
        cin >>choose;
        break;
    }
}

void viewActions(){
    int choose;
    
    cout<<"\t\t\t+++++++++++++++++++\n";
    cout<<"\t\t\tSearch item\n";
    cout<<"\t\t\t+++++++++++++++++++\n\n";
    
    cout<<"\t\t\tSelect [1] to enter Search details\n";
    cout<<"\t\t\tSelect [0] to Exit\n\n";
    cout<<"\t\t\tID must be in the inventory to be displayed(e.g, [CD100])\n";
    cin>>choose;
    
    switch(choose){
        case 1:
        {
        string s;
        cout<<"Enter the ID of the product: \n";
        cin.get();
        getline(cin, s);
        
        printItem(s);//output the value with this string
        viewActions();
        }
        
        break;
        
        case 0:
        return;
        
        break;
        default:
        cout<<"\t\t\t+++++++++++\n";
        cout<<"\t\t\tSearch item\n";
        cout<<"\t\t\t+++++++++++\n\n";
        
        cout<<"\t\t\tSelect [1] to enter Search details\n";
        cout<<"\t\t\tSelect [0] to Exit\n\n";
        cout<<"\t\t\tID must be in the inventory to be displayed(e.g, [CD100])\n";
        cin>>choose;
        break;
    }
    
    
    
}


void inv(){//stock inventory
    int choose;
    
    cout<<"\t\t\t+++++++++++++++++++++++++++\n";
    cout<<"\t\t\tThis is the Stock Inventory\n";
    cout<<"\t\t\t+++++++++++++++++++++++++++\n\n";
    
    cout<<"\t\t\tSelect [1] to INSERT Item\n";
    cout<<"\t\t\tSelect [2] to SEARCH for item in stock\n";
    cout<<"\t\t\tSelect [3] to UPDATE stocks\n";
    cout<<"\t\t\tSelect [0] to Exit\n\n";
    
    cout<<"\t\t\tSelect your choice: ";
    cin >>choose;
    
    switch(choose){
        case 1:
        addActions();
        inv();
        break;
        
        case 2:
        viewActions();
        inv();
        break;
        
        case 3:
        updateActions();
        inv();
        break;
        
        case 0:
        return;
        break;
        
        default:
        cout<<"\t\t\t+++++++++++++++++++++++++++\n";
        cout<<"\t\t\tThis is the Stock Inventory\n";
        cout<<"\t\t\t+++++++++++++++++++++++++++\n\n";
        
        cout<<"\t\t\tSelect [1] to INSERT Item\n";
        cout<<"\t\t\tSelect [2] to SEARCH for item in stock\n";
        cout<<"\t\t\tSelect [3] to UPDATE stocks\n";
        cout<<"\t\t\tSelect [0] to Exit\n\n";
        
        cout<<"\t\t\tSelect your choice: ";
        cin >>choose;
        break;
    }
}


    
void staffPass(){
    int pass;
    
    cout<<"\t\t\t+++++++++++++++\n";
    cout<<"\t\t\tSecurity Access\n";
    cout<<"\t\t\t+++++++++++++++\n";
    cout<<"\t\t\tSelect [0] to Exit\n\n";
    cout<<"\t\t\tEnter 4-digit Pass: ";
    cin >>pass;
    
    int acc = 1234;
    
    while(pass!=acc && pass !=0){
        cout<<"\t\t\t+++++++++++++++\n";
        cout<<"\t\t\tAccess Denied\n";
        cout<<"\t\t\t+++++++++++++++\n";
        cout<<"\t\t\tSelect [0] to Exit\n\n";
        cout<<"\t\t\tEnter 4-digit Pass: ";
        cin >>pass;
    }
    
    if(pass == acc){
        inv();
        return;
    }
    if (pass == 0){
        return;
    }
}

int total =0;

void invoice(){
    cout<<"+++++++\n";
    cout<<"INVOICE\n";
    cout<<"+++++++\n\n";
    
    ifstream read;
    read.open("invoice.txt", ios::in);
    
    item i;
    i = getStock(read);
    while(!read.eof()){
        cout<<"\n"<<i.stockID<<"----"<<i.stockName<<"----$"<<i.price<<"----"<<i.quantity<<"\n";
        total+=i.price;
        i =getStock(read);
    }
    cout<<"TOTAL: $"<<total;

}



void sellItem(){
    string id;
    int q = 1;
    
    int choose;
    
    ifstream in("invoice.txt", ios::in);
    ofstream print("invoice.txt", ios::app);
    
    
    in.seekg(0, ios::end);  
    if (in.tellg() == 0) {    
        print<<endl; 
    }
     ifstream temp("temp.txt", ios::in);
    ofstream write("temp.txt", ios::app);
    write<<endl;
    
    printStock();
    cout<<"\nEnter product id: ";
    cin.get();
    getline(cin, id);
    
    ifstream read;
    read.open("stock.txt", ios::in);
    item i;
    
    i = getStock(read);
    while(!read.eof()){
        
        if (i.stockID ==id){
            
            print<< i.stockID << endl;
            print<< i.stockName;
            print<< endl << i.price << endl << q <<endl;
            
            i.quantity -= q;
            
            temp.seekg(0, ios::end);
            if (temp.tellg() == 0) {    
                write<<endl; 
            }
            
            write<< i.stockID << endl;
            write<< i.stockName;
            write<< endl << i.price << endl << i.quantity<<endl;
            
            cout<<"Item added\n";
            cout<<"CHEKOUT?\n";
            cout<<"\t[1] for yes\n";
            cout<<"\t[2] for no\n";
            cin>>choose;
            
            switch(choose){
                case 1:
                {
                    remove("stock.txt");
                    rename("temp.txt", "stock.txt");
                    invoice();
                }

                break;
                
                case 2:
                {
                    remove("stock.txt");
                    rename("temp.txt", "stock.txt");
                    sellItem();
                }
                     
                
                break;
                
                default:
                cout<<"Item added\n";
                cout<<"CHEKOUT?\n";
                cout<<"\t[1] for yes\n";
                cout<<"\t[2] for no\n";
                cin>>choose;
                break;
            }
            
            
        }else if(read.eof()){
            write<< i.stockID << endl;
            write<< i.stockName;
            write<< endl << i.price << endl << i.quantity<<endl;
             cout<<"Product not found";
                sellItem();
        }
            
        write<< i.stockID << endl;
        write<< i.stockName;
        write<< endl << i.price << endl << i.quantity<<endl;
        
        i = getStock(read);
    }
    remove("stock.txt");
    rename("temp.txt", "stock.txt");
    

}


void menu(){
    int choose;
    
        cout<<"\t\t\t+++++++++++++++++++++++++\n";
        cout<<"\t\t\tWelcome to the Music Shop\n";
        cout<<"\t\t\t+++++++++++++++++++++++++\n\n";
        
        cout<<"\t\t\tSelect [1] to Make a Purhase\n";
        cout<<"\t\t\tSelect [2] to View Stock Management System\n";
        cout<<"\t\t\tSelect [0] to Exit\n\n";
        cout<<"\t\t\tSelect your choice: ";
        cin >>choose;
    
        
        switch(choose){
            case 1:
            sellItem();
            break;
            
            case 2:
            staffPass();
            menu();
            break;
            
            case 0:
            cout<<"Thanks for coming!";
            return;
            break;
            
            default:
            menu();
        }
        
        
}
    //Base class - store
class Store{
    
    int choose;
    
    public:
    void start(){
        menu();
    }
    
};



int main()
{
    ofstream write("stock.txt", ios::app);
    ifstream read("stock.txt", ios::in);
    
    read.seekg(0, ios::end);  
    if (read.tellg() == 0) {    
        write<<endl; 
    }
    Store s;
    s.start();
    return 0;
}
