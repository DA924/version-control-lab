#include <iostream>
#include <string.h>
#include <conio.h>

using namespace std;

struct itemEntry{
    float unit_price;
    int copies;
    int product_id;
    char name[30];
    char company[30];
};

class Store{
    public:
    int numItem;
    itemEntry database[100];
    
    Store(){
        numItem = 0;
    }
    void insertItem(char itemName[],char company[],int pid, int c, float p);
    void deleteItem(char itemName[],int pid);
    itemEntry *searchi(char itemName[],int pid);
    void updateItem(char itemName[], int pid, int total, float price);
    
};

void Store::insertItem(char itemName[],char company[],int pid, int c, float p){
    strcpy(database[numItem].name,itemName);
    strcpy(database[numItem].company,company);
    database[numItem].product_id=pid;
    database[numItem].copies=c;
    database[numItem].unit_price=p;
    cout<<"\n\t\t ITEM INSERTED SUCESSFULLY \n";
    numItem++;
}

void Store ::deleteItem(char itemName[],int pid){
    int i;
    for(i=0;i<numItem;i++){
        if((strcmp(itemName,database[i].name)==0) && (database[i].product_id==pid)){
            database[i].copies--;
            cout<<"\n\t\t ITEM DELETED SUCESSFULLY \n";
            return;
        }
            
    }
    cout<<"\n\t\t ITEM NOT FOUND \n";
    
}

itemEntry *Store::searchi(char itemName[],int pid){
    int i;
    for(i=0;i<numItem;i++){
        if((strcmp(itemName,database[i].name)==0) && (database[i].product_id==pid))
            return &database[i];
        
    }
    return NULL;
}

void Store::updateItem(char itemName[], int pid, int total, float price){
    itemEntry *item=searchi(itemName,pid);
    if(item==NULL){
        cout<<"\n\t\t ITEM NOT FOUND \n";
        return;
    }
    item ->copies+=total;
    item ->unit_price=price;
}

int main(){
    
    Store sto;
    char name[30],company[30];
    int product_id,copies,unit_price,option;
    while(option!=5){
        cout<<"\n\t\t DEPARTMENT STORE \n";
        cout<<"\n\t\t MENU \n";
        cout<<"\n\t\t 1 - INSERT \n";
        cout<<"\n\t\t 2 - DELETE \n";
        cout<<"\n\t\t 3 - SEARCH \n";
        cout<<"\n\t\t 4 - UPDATE \n";
        cout<<"\n\t\t 5 - EXIT \n";
        cout<<"\n\t\t Enter your choice: \n";
        cin>>option;
        
        switch(option){
            case 1:
                cin.getline(name,80);
                cout<<"\n\t\t\t Enter Name of Item: ";
                cin.getline(name,80);
                cout<<"\n\t\t\t Enter Company: ";
                cin.getline(company,80);
                cout<<"\n\t\t\t Enter Product ID: ";
                cin>>product_id;
                cout<<"\n\t\t\t no of copies: ";
                cin>>copies;
                cout<<"\n\t\t\t Unit price: ";
                cin>>unit_price;
                sto.insertItem(name,company,product_id,copies,unit_price);
                break;
            
            case 2:
                cin.getline(name,80);
                cout<<"\n\t\t\t Enter Name of Item: ";
                cin.getline(name,80);
                cout<<"\n\t\t\t Enter Product ID: ";
                cin>>product_id;
                sto.deleteItem(name, product_id);
                break;
            case 3:
                cin.getline(name,80);
                cout<<"\n\t\t\t Enter Name of Item: ";
                cin.getline(name,80);
                cout<<"\n\t\t\t Enter Product ID: ";
                cin>>product_id;
                itemEntry *test;
                test = sto.searchi(name,product_id);
                if(test!=NULL){
                    cout<<"\n\tSearching Result...";
                    cout<<"\n\t\t\t ITEM FOUND"
                    <<"\n\t\t\tName of Item: "<<test->name
                    <<"\n\t\t\tName of company: "<<test->company
                    <<"\n\t\t\tProduct id: "<<test->product_id
                    <<"\n\t\t\tNumber of copies: "<<test->copies
                    <<"\n\t\t\tUnit price: "<<test->unit_price;
                }else
                    cout<<"\n\t\t\tITEM NOT FOUND ";
                break;
            case 4:
                cout<<"\n\t\t\t Enter details for update ";
                cin.getline(name,80);
                cout<<"\n\t\t\t Enter Name of Item: ";
                cin.getline(name,80);
                cout<<"\n\t\t\t Enter Product ID: ";
                cin>>product_id;
                cout<<"\n\t\t\t Enter total new entry: ";
                cin>>copies;
                cout<<"\n\t\t\t Enter new price: ";
                cin>>unit_price;
                sto.updateItem(name,product_id,copies,unit_price);
                break;
        }
    }
    return 0;
}